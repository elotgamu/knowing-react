import * as React from "react";

import { Link } from "react-router-dom";

import { getCountryStatistics } from "../../services/covidApi";

const HomePage = () => {
  const [countries, setCountriesData] = React.useState({
    data: null,
    loading: false,
    error: null,
  });

  //With empty array of dependencies
  // fetch the api data when component
  // `mount`
  React.useEffect(() => {
    const getData = async () => {
      try {
        setCountriesData((c) => ({ ...c, loading: true }));
        const response = await getCountryStatistics();
        const data = response.response;
        setCountriesData((c) => ({
          ...c,
          loading: false,
          error: null,
          data: data,
        }));
      } catch (error) {
        setCountriesData((c) => ({
          ...c,
          error: error?.message,
          loading: false,
        }));
      }
    };

    getData();
  }, []);

  return (
    <main className="ui container">
      <p>This is the home page</p>

      {countries.loading && (
        <div className="ui active dimmer">
          <p className="ui text loader">Loading...</p>
        </div>
      )}

      {countries.error ? (
        <section>
          <p>{countries.error}</p>
        </section>
      ) : null}

      {countries.data ? (
        <table className="ui blue table">
          <thead>
            <tr>
              <th>Country</th>
              <th>Continent</th>
              <th>Population</th>
            </tr>
          </thead>
          <tbody>
            {countries.data.map((country) => {
              return (
                <tr key={country.country}>
                  <td>
                    <Link to={`/${country.country}`}>{country.country}</Link>
                  </td>
                  <td>{country.continent}</td>
                  <td>{country.population}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      ) : null}
    </main>
  );
};

export default HomePage;
