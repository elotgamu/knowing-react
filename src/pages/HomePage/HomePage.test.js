import * as React from "react";
import { MemoryRouter } from "react-router-dom";

import { render, screen, waitFor } from "@testing-library/react";

import HomePage from "../HomePage";

import { allCountries } from "../../fixtures/CovidAPIResponse";

describe("requests", () => {
  let originalFetch;

  beforeAll(() => {
    originalFetch = global.fetch;
    global.fetch = jest.fn().mockImplementation(() =>
      Promise.resolve({
        ok: true,
        json: () => Promise.resolve(),
      })
    );
  });

  beforeEach(() => {
    jest.clearAllMocks();
  });

  afterAll(() => {
    global.fetch = originalFetch;
  });

  it("should allow user to fetch country data", async () => {
    global.fetch.mockImplementationOnce(() =>
      Promise.resolve({
        ok: true,
        json: () => Promise.resolve(allCountries),
      })
    );
    render(<HomePage />, { wrapper: MemoryRouter });

    await waitFor(() =>
      expect(screen.queryByText(/Loading/i)).not.toBeInTheDocument()
    );

    const ourCountry = screen.getByText("Nicaragua");
    expect(ourCountry).toBeInTheDocument();
  });

  it("should let user know about errors", async () => {
    global.fetch.mockImplementationOnce(() =>
      Promise.reject(new Error("Error fetching Data"))
    );
    render(<HomePage />, { wrapper: MemoryRouter });

    await waitFor(() =>
      expect(screen.queryByText(/Loading/i)).not.toBeInTheDocument()
    );

    expect(screen.getByText("Error fetching Data")).toBeInTheDocument();
    expect(screen.queryByText("Nicaragua")).not.toBeInTheDocument();
  });
});
