import * as React from "react";
import { Link, useParams } from "react-router-dom";

import { getCountryStatistics } from "../../services/covidApi";

const DetailPage = () => {
  const [countryData, setCountryData] = React.useState({
    data: null,
    loading: false,
    error: null,
  });
  const { countryName } = useParams();

  const getDetails = async (country) => {
    try {
      setCountryData((c) => ({ ...c, loading: true }));
      const data = await getCountryStatistics(country);
      setCountryData((c) => ({
        ...c,
        data: data.response[0],
        loading: false,
        error: null,
      }));
    } catch (error) {
      setCountryData((c) => ({
        ...c,
        loading: false,
        error: error,
      }));
    }
  };

  // useful to fetch country based on current
  // path parameter
  React.useEffect(() => {
    if (!countryName) {
      return;
    }
    getDetails(countryName);
  }, [countryName]);

  return (
    <main className="ui container">
      <p>
        <Link to="/" title="Go back to home page">
          Go back to home page
        </Link>
      </p>
      <h3 className="ui header">This is the detail page for {countryName}</h3>

      {countryData.loading && (
        <div className="ui active dimmer">
          <p className="ui text loader">Loading...</p>
        </div>
      )}

      <section>
        <div className="ui three column stackable grid">
          <div className="column">
            <div className="ui raised segment">
              <h4 className="ui header">Cases:</h4>
              <div>
                <p>New: {countryData.data?.cases?.new}</p>
                <p>Active: {countryData.data?.cases?.active}</p>
                <p>Recovered: {countryData.data?.cases?.recovered}</p>
              </div>
            </div>
          </div>

          <div className="column">
            <div className="ui raised segment">
              <h4 className="ui header">Tests:</h4>
              <div>
                <p>1M: {countryData.data?.tests?.["1M_pop"]}</p>
                <p>Total: {countryData.data?.cases?.total}</p>
              </div>
            </div>
          </div>

          <div className="column">
            <div className="ui raised segment">
              <h4 className="ui header">Deaths: </h4>
              <div>
                <p>New: {countryData.data?.deaths?.new}</p>
                <p>1M: {countryData.data?.deaths?.["1M_pop"]}</p>
                <p>Total: {countryData.data?.deaths?.total}</p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  );
};

export default DetailPage;
