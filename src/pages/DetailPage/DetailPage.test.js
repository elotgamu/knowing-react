import * as React from "react";
import { render, screen, waitFor } from "@testing-library/react";

import DetailPage from "../DetailPage";
import { MemoryRouter, Routes, Route } from "react-router-dom";
import { detailResponse } from "../../fixtures/CovidAPIResponse";

describe("request to API", () => {
  let originalFetch;

  beforeAll(() => {
    originalFetch = global.fetch;
    global.fetch = jest.fn().mockImplementation(() =>
      Promise.resolve({
        ok: true,
        json: () => Promise.resolve(),
      })
    );
  });

  beforeEach(() => {
    jest.clearAllMocks();
  });

  afterAll(() => {
    global.fetch = originalFetch;
  });

  it("should allow user to get Country data", async () => {
    global.fetch.mockImplementationOnce(() =>
      Promise.resolve({
        ok: true,
        json: () => Promise.resolve(detailResponse),
      })
    );
    render(
      <MemoryRouter initialEntries={["/Nicaragua"]}>
        <Routes>
          <Route path="/:countryName" element={<DetailPage />} />
        </Routes>
      </MemoryRouter>
    );

    const title = screen.getByText("This is the detail page for Nicaragua");
    expect(title).toBeInTheDocument();
    await waitFor(() =>
      expect(screen.queryByText(/Loading/i)).not.toBeInTheDocument()
    );

    expect(screen.getByText("Active: 14041")).toBeInTheDocument();
  });

  it("should not invoke API if no country provided", () => {
    render(<DetailPage />, { wrapper: MemoryRouter });

    expect(global.fetch).toHaveBeenCalledTimes(0);
    expect(
      screen.queryByText("This is the detail page for Nicaragua")
    ).not.toBeInTheDocument();
  });
});
