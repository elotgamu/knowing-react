import { Route, Routes } from "react-router-dom";

import logo from "./logo.svg";
import "./App.css";

import HomePage from "./pages/HomePage";
import DetailPage from "./pages/DetailPage";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      <div>
        <p>Our app</p>

        <Routes>
          <Route path="/:countryName" element={<DetailPage />} />
          <Route path="/" element={<HomePage />} />
        </Routes>
      </div>
    </div>
  );
}

export default App;
