import * as React from "react";

/**
 *
 * Table to render a list of users
 * @type {React.FC}
 * @param {Object[]}  props.users 'The list of users
 *
 */
const UserTable = ({ users = [] }) => {
  return (
    <table className="ui blue table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Email</th>
          <th>Phone</th>
        </tr>
      </thead>
      <tbody>
        {users.map((user) => {
          return (
            <tr key={user.id}>
              <td>{user.name}</td>
              <td>{user.email}</td>
              <td>{user.phone}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default UserTable;
