import * as React from "react";
import { useState } from "react";

// components
import Loader from "../Loader";
import UserTable from "../UsersTable";

const UserList = () => {
  const [data, setData] = useState({
    users: null,
    loading: false,
    error: "test",
  });

  const getUsers = async () => {
    setData({ ...data, loading: true });
    const response = await fetch("https://jsonplaceholder.typicode.com/users");
    const users = await response.json();
    setData({ ...data, users: users, loading: false });
  };

  return (
    <section className="ui container">
      <p>List of user</p>
      <button type="button" onClick={() => getUsers()} disabled={data.loading}>
        Get User
      </button>

      {data.loading && <Loader isLoading={data.loading} />}
      {data.users ? <UserTable users={data.users} /> : null}
    </section>
  );
};

export default UserList;
