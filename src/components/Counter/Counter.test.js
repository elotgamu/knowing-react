import * as React from "react";

import userEvent from "@testing-library/user-event";
import { render, screen } from "@testing-library/react";

import Counter from "../Counter";

describe("render component", () => {
  it("should render without crash", () => {
    render(<Counter />);
    const text = screen.getByText("My Counter");
    expect(text).toBeInTheDocument();
  });

  it("should match snapshot", () => {
    const { container } = render(<Counter />);
    expect(container).toMatchSnapshot();
  });
});

describe("user interaction", () => {
  it("should allow user to increment values", () => {
    render(<Counter />);
    const input = screen.getByLabelText("Counter");
    const increaseButton = screen.getByText("Aumentar", { selector: "button" });
    // const increaseButton = screen.getByRole("button", { name: "Aumentar" });
    expect(increaseButton).toBeInTheDocument();
    expect(input).toHaveDisplayValue("0");
    userEvent.click(increaseButton);
    expect(input).toHaveDisplayValue("1");
  });

  it("should allow user to  decrease values", () => {
    render(<Counter />);
    const input = screen.getByLabelText("Counter");
    const increaseButton = screen.getByText("Aumentar", {
      selector: "button",
    });
    const decreaseButton = screen.getByText("Disminuir", {
      selector: "button",
    });
    expect(increaseButton).toBeInTheDocument();
    expect(input).toHaveDisplayValue("0");
    userEvent.click(increaseButton);
    expect(input).toHaveDisplayValue("1");
    userEvent.click(decreaseButton);
    expect(input).toHaveDisplayValue("0");
  });
});
