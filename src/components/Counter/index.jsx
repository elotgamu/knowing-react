import * as React from "react";
import { useState } from "react";

const Counter = () => {
  const [counter, setCounter] = useState(0);

  const onChange = (e) => {
    setCounter(e.target.valueAsNumber);
  };

  const onAdd = () => {
    setCounter(counter + 1);
  };

  const onSubtract = () => {
    setCounter(counter - 1);
  };

  return (
    <section id="counter-section">
      <p>My Counter</p>
      <label htmlFor="counter">Counter</label>
      <input
        type="number"
        value={counter}
        id="counter"
        name="counter"
        onChange={onChange}
      />

      <button type="button" onClick={() => onAdd()}>
        Aumentar
      </button>
      <button type="button" onClick={() => onSubtract()}>
        Disminuir
      </button>
    </section>
  );
};

export default Counter;
