import * as React from "react";
import { useState } from "react";

const List = () => {
  const [users, setUsers] = useState([]);
  const [data, setData] = useState("");

  const onChange = (e) => {
    console.log("element ", e);
    console.log("value ", e.target.value);
    setData(e.target.value);
  };

  const onSubmit = (e) => {
    e.preventDefault();
    setUsers([...users, data]);
    setData("");
  };

  return (
    <section>
      <p>List component</p>

      <form>
        <label htmlFor="user">User</label>
        <input
          id="user"
          name="user"
          type="text"
          value={data}
          onChange={onChange}
        />
        <button type="submit" onClick={(e) => onSubmit(e)}>
          Add
        </button>
      </form>

      <p>List of users:</p>
      <ul>
        {users.map((user) => (
          <li key={user}>{user}</li>
        ))}
      </ul>
    </section>
  );
};

export default List;
