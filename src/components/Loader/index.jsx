import * as React from "react";

/**
 * @type {React.FC}
 * @param {boolean} props.isLoading
 *
 */
const Loader = ({ isLoading }) => {
  return isLoading ? (
    <div class="ui segment">
      <div class="ui active inverted dimmer">
        <div class="ui text loader">Loading</div>
      </div>
      <p></p>
    </div>
  ) : null;
};

export default Loader;
