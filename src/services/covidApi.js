/**
 * [getCountryStatistics description]
 *
 * @param   {string}  country  Query param to filter by country
 *
 * @return  {Promise<Object>}
 */
export const getCountryStatistics = async (country) => {
  const RAPID_API_HOST = process.env.REACT_APP_RAPID_API_HOST || "";
  const RAPID_API_KEY = process.env.REACT_APP_RAPID_API_KEY || "";
  const BASE_URL = "https://covid-193.p.rapidapi.com/statistics";
  const url = new URL(BASE_URL);

  if (country) {
    url.search = new URLSearchParams({ country: country });
  }

  const path = url.toString();
  const headers = {
    "X-RapidAPI-Host": RAPID_API_HOST,
    "X-RapidAPI-Key": RAPID_API_KEY,
  };
  const response = await fetch(path, { headers: headers });
  const data = await response.json();
  return data;
};
