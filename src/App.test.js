import { render, screen } from "@testing-library/react";

import { MemoryRouter } from "react-router-dom";
import App from "./App";

test("renders learn react link", () => {
  // render(<App />, { wrapper: MemoryRouter });
  render(
    <MemoryRouter>
      <App />
    </MemoryRouter>
  );

  const text = screen.getByText("This is the home page");
  expect(text).toBeInTheDocument();
});
